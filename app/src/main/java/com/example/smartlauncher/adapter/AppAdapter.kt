package com.example.smartlauncher.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.smartlauncher.R
import com.example.smartlauncher.databinding.ItemAppBinding
import com.example.launchersdk.AppObject

public class AppAdapter(var context: Context, var appList: List<AppObject>, var appAdapterClicks: AppAdapterClicks) :
    RecyclerView.Adapter<AppAdapter.MyViewHolder>() {

    class MyViewHolder(itemAppBinding: ItemAppBinding) :
        RecyclerView.ViewHolder(itemAppBinding.root) {
        val mItemAppBinding: ItemAppBinding = itemAppBinding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemAppBinding: ItemAppBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_app, parent, false)
        return MyViewHolder(itemAppBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val appObject = appList[position]
        holder.mItemAppBinding.icon.setImageDrawable(appObject.image)
        holder.mItemAppBinding.label.text = appObject.name
        holder.mItemAppBinding.appItemLayout.setOnClickListener {
            appAdapterClicks.onItemClick(appObject)
        }
        holder.mItemAppBinding.appItemLayout.setOnLongClickListener {
            appAdapterClicks.onItemLongClick(appObject)
        }
    }

    override fun getItemCount(): Int {
        return appList.size
    }

    interface AppAdapterClicks{
        fun onItemClick(appObject: AppObject)
        fun onItemLongClick(appObject: AppObject) : Boolean
    }

}