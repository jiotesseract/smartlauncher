package com.example.smartlauncher.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.launchersdk.AppObject
import com.example.launchersdk.AppUtility.updateInstalledAppList

class AppViewModel : ViewModel() {

    val liveAppList: MutableLiveData<MutableList<AppObject>> = MutableLiveData()
    private val installedAppList: MutableList<AppObject> = arrayListOf()
    private val filteredAppList: MutableList<AppObject> = arrayListOf()

    init {
        liveAppList.value = arrayListOf()
    }

    fun updateLiveAppList(context: Context) {
        installedAppList.clear()
        installedAppList.addAll(updateInstalledAppList(context))
        liveAppList.value = installedAppList
    }

    fun filterListUsingQuery(query: String) {
        filteredAppList.clear()
        for (app in installedAppList) {
            if (app.name.contains(query, true)) {
                filteredAppList.add(app)
            }
        }
        liveAppList.value = filteredAppList
    }
}