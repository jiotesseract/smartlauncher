package com.example.smartlauncher.views

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.launchersdk.AppObject
import com.example.launchersdk.AppUtility.hideKeyboard
import com.example.launchersdk.AppUtility.launchApp
import com.example.launchersdk.AppUtility.openAppSettings
import com.example.launchersdk.AppUtility.uninstallApp
import com.example.launchersdk.DialogManager
import com.example.smartlauncher.*
import com.example.smartlauncher.adapter.AppAdapter
import com.example.smartlauncher.databinding.ActivityMainBinding
import com.example.smartlauncher.viewmodels.AppViewModel


class MainActivity : AppCompatActivity(), AppAdapter.AppAdapterClicks {

    private lateinit var dataBinding: ActivityMainBinding;
    private lateinit var appAdapter: AppAdapter
    private lateinit var searchQueryRunnable: Runnable
    private lateinit var searchQueryHandler: Handler
    private var installedAppList: ArrayList<AppObject> = ArrayList()
    private var searchQueryForApi = ""
    private lateinit var viewModel: AppViewModel
    private lateinit var appReceiver: BroadcastReceiver


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(AppViewModel::class.java)
        viewModel.updateLiveAppList(applicationContext)

        initSearchView()
        initRecycleView()
        initObservers()
    }

    override fun onResume() {
        super.onResume()
        registerPackageUninstallReciever()
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(appReceiver)
    }


    private fun showAppOptions(appObject: AppObject): Boolean {
        val message = "\nPackage name: ${appObject.packageName}\n\n" +
                "Version Code: ${appObject.versionCode}\n\n" +
                "Version Name: ${appObject.versionName}\n\n" +
                "Class Name: ${appObject.className}\n"

        DialogManager.threeBtnDialog(this,
            appObject.name,
            message,
            "Info",
            "Uninstall",
            "Cancel",
            object : DialogManager.ThreeBtnDialogListener {
                override fun onPositiveBtnPress() {
                    openAppSettings(appObject.packageName, this@MainActivity)
                }

                override fun onNegativeBtnPress() {
                    uninstallApp(appObject.packageName, this@MainActivity)
                }

                override fun onNeutralBtnPress() {

                }
            },
            appObject.image
        )
        return true
    }

    private fun showToast(message: String) {
        Toast.makeText(
            applicationContext,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun registerPackageUninstallReciever() {
        appReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent) {
                val packageName: String? = intent.data?.encodedSchemeSpecificPart
                when (intent.action) {
                    Intent.ACTION_PACKAGE_REMOVED -> {
                        showToast("App Removed: ${packageName ?: ""}")
                    }
                    Intent.ACTION_PACKAGE_ADDED -> {
                        showToast("App Added: ${packageName ?: ""}")
                    }
                }
                viewModel.updateLiveAppList(applicationContext)
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED)
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED)
        intentFilter.addDataScheme("package")
        registerReceiver(appReceiver, intentFilter)
    }

    private fun initObservers() {
        viewModel.liveAppList.observe(this, Observer {
            installedAppList.clear()
            installedAppList.addAll(it)
            appAdapter.notifyDataSetChanged()
        })
    }

    private fun initSearchView() {
        searchQueryHandler = Handler()
        searchQueryRunnable = Runnable {
            performSearchOverQuery(searchQueryForApi)
            searchQueryHandler.removeCallbacks(searchQueryRunnable)
        }
        setSearchListener()
    }

    private fun performSearchOverQuery(query: String) {
        viewModel.filterListUsingQuery(query)
    }

    private fun setSearchListener() {
        dataBinding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchQueryHandler.removeCallbacks(searchQueryRunnable)
                performSearchOverQuery(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                searchQueryForApi = newText
                searchQueryHandler.removeCallbacks(searchQueryRunnable)
                searchQueryHandler.postDelayed(searchQueryRunnable, 1000)
                return false
            }
        })
    }

    private fun initRecycleView() {
        appAdapter = AppAdapter(applicationContext, installedAppList, this)
        dataBinding.recycleView.layoutManager = GridLayoutManager(this, 4)
        dataBinding.recycleView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING)
                    hideKeyboard(this@MainActivity)
            }
        })
        dataBinding.recycleView.adapter = appAdapter
    }

    override fun onItemClick(appObject: AppObject) {
        launchApp(appObject.packageName, applicationContext)
    }

    override fun onItemLongClick(appObject: AppObject): Boolean {
        showAppOptions(appObject)
        return true
    }


}