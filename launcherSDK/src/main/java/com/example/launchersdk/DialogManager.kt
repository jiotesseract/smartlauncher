package com.example.launchersdk

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.Drawable

object DialogManager {

    fun threeBtnDialog(
        context: Context?,
        title: String = "",
        message: String = "",
        positiveBtnText: String = "Yes",
        negativeBtnText: String = "No",
        neutralBtnText: String = "Cancel",
        threeBtnDialogListener: ThreeBtnDialogListener,
        image: Drawable
    ) {
        if (context != null) {
            val builder = AlertDialog.Builder(context)
            builder.setCancelable(false)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setIcon(image)
            builder.setPositiveButton(
                positiveBtnText
            ) { dialog: DialogInterface?, which: Int -> threeBtnDialogListener.onPositiveBtnPress() }

            builder.setNegativeButton(
                negativeBtnText
            ) { dialog: DialogInterface?, which: Int -> threeBtnDialogListener.onNegativeBtnPress() }

            builder.setNeutralButton(
                neutralBtnText
            ) { dialog: DialogInterface?, which: Int -> threeBtnDialogListener.onNeutralBtnPress() }
            builder.create().show()
        }
    }

    interface ThreeBtnDialogListener {
        fun onPositiveBtnPress()
        fun onNegativeBtnPress()
        fun onNeutralBtnPress()
    }
}