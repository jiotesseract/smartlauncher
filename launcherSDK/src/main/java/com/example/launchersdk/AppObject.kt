package com.example.launchersdk

import android.graphics.drawable.Drawable

data class AppObject(val name: String, val packageName: String, val image: Drawable,
                     val versionCode : String, val versionName: String, val className: String) : Comparable<AppObject>{
    override fun compareTo(other: AppObject): Int = this.name.compareTo(other.name)

}