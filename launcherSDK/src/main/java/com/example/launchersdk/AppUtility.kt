package com.example.launchersdk

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.ResolveInfo
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager

object AppUtility {

        fun launchApp(packageName: String, context: Context) {
            val intent: Intent? = context.packageManager.getLaunchIntentForPackage(packageName)
            if (intent != null) {
                context.startActivity(intent)
            }
        }

        fun openAppSettings(packageName: String, context: Activity) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri: Uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            context.startActivity(intent)
        }

        fun uninstallApp(packageName: String, context: Activity) {
            val intent = Intent(Intent.ACTION_DELETE)
            intent.data = Uri.parse("package:$packageName")
            context.startActivity(intent)
        }

        fun hideKeyboard(activity: Activity?) {
            if (activity == null) {
                return
            }
            val imm: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view: View? = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        }

        fun updateInstalledAppList(applicationContext: Context): MutableList<AppObject> {
            val installedAppList: MutableList<AppObject> = arrayListOf()

            val intent = Intent(Intent.ACTION_MAIN, null)
            intent.addCategory(Intent.CATEGORY_LAUNCHER)

            val untreatedAppList: List<ResolveInfo> =
                applicationContext.packageManager.queryIntentActivities(intent, 0)

            for (untreatedApp in untreatedAppList) {
                val appName =
                    untreatedApp.activityInfo.loadLabel(applicationContext.packageManager).toString()
                val appPackageName = untreatedApp.activityInfo.packageName.toString()
                val appIcon: Drawable =
                    untreatedApp.activityInfo.loadIcon(applicationContext.packageManager)
                val pInfo: PackageInfo =
                    applicationContext.packageManager.getPackageInfo(appPackageName, 0)
                val mainIntent = applicationContext.packageManager.getLaunchIntentForPackage(
                    appPackageName
                )
                val className: String = mainIntent?.component!!.className
                val versionCode = pInfo.versionCode.toString()
                val versionName = pInfo.versionName
                val appObject = AppObject(
                    appName,
                    appPackageName,
                    appIcon,
                    versionCode,
                    versionName,
                    className
                )

                if (!installedAppList.contains(appObject))
                    installedAppList.add(appObject)
            }
            installedAppList.sort()
            return installedAppList
        }
}